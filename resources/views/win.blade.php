<html>
    <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
          <link rel="stylesheet" href="css/style.css">
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
           <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.2/jquery.ui.touch-punch.min.js"></script>
          <script>
        </script>
    </head>
    
    <body>
        
        <script>
          window.fbAsyncInit = function() {
            FB.init({
              appId      : '1484940991537238',
              xfbml      : true,
              version    : 'v2.9'
            });
            //FB.AppEvents.logPageView();
            
            FB.getLoginStatus(function(response) {
              if (response.status === 'connected') {
                console.log(response.authResponse.accessToken);
              }
              else if(response.status === 'not_authorized'){
                  console.log(response.status);
                  FB.login(function(response){
                      if (response.status === 'connected') {
                        console.log(response.authResponse.accessToken);
                          FB.api('/me',{fields: 'name, email' },function(response) {
                                console.log("ddd" + JSON.stringify(response));
                                // send data to google form api
                                $.post("https://docs.google.com/forms/d/e/1FAIpQLSfnESIY9kdcIoGBPEAZn68iTQ1Y_5MFpq4QZS8_-SHoj8VPWg/formResponse",
                                {
                                    'entry.1536132544' : response.name,
                                    'entry.1933746222': response.email,
                                    'entry.1081999847' : response.id
                                },
                                function(data, status){
                                    alert("Data: " + data + "\nStatus: " + status);
                                });
                            });
                
                                
                      }
                      else if(response.status === 'not_authorized'){
                          console.log(response.status);
                          location.reload();
                      }
                      else{
                          console.log(response.status);
                      }
                      
                  },{scope: 'email'});
                  
                  FB.api('/me/permissions', function(response) {
                                console.log("dddmm" + JSON.stringify(response));
                            });
              }
              else{
                  console.log(response.status);
              }
            });
          };

          (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "https://connect.facebook.net/en_US/all.js";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));
        </script>
        <!--background:url(FBimages/bg.png) no-repeat;height:100%;width:100-->
        
        <div class="cont">
        <div class="container-fluid">
            <div class="row" style=" padding-top:6%;" >
                <div class="col-sm-6 col-sm-push-6 brief">
                    <img src="FBimages/Roche_logo.png" width="72" height="38" class="roche-logo">
                    <div>
                        <h1>اتعلم خطوات أكيو-تشيك لقياس السكر بالدم</h1>
                        <p>قم بوضع الصور حسب الترتيب الصحيح لخطوات قياس السكر بالدم واحصل على فرصة للحصول على جهاز أكيو-تشيك بيرفورما.</p>
                    </div>
                    
                </div>
                
                <!-- ITEMS COLUM -->
                <div class="col-sm-6 col-sm-pull-6" id="items">
                    <!-- ITEMS -->
                    <div class="items">
                        <!-- ROW 1 -->
                        <div class="row"  >
                            <div class="col-sm-6" >
                                <div style="height:216px;" id="device_before" >
                                    <img src="FBimages/device2.png" id="dev_image" draggable="false"  style="margin-left: auto; margin-right: auto;display: block;padding-top: 32px" >
                                    
                                </div>
                            </div>
                            <div class="col-sm-6" style="background-color:;" >
                                <div style="height:216px;cursor: pointer;" id="pin_div">
                                    <img src="FBimages/pin.png" id="pin"   draggable="true" " style="margin-left: auto; margin-right: auto;display: block;padding-top: 25px;z-index: 9999999;" >
                                </div>
                            </div>
                        </div>
                        <!-- ./ROW 1 -->
                    
                        <!-- ROW 2 -->
                        <div class="row margin-bottom" style="padding-top:0%">
                            <div class="col-sm-6" style="background-color:;">
                                <div style="cursor:pointer;" id="hand_div">
                                    <img style="margin-top: 32px;" src="FBimages/hand.png" id="hand"  draggable="true" >
                                </div>
                            </div>
                            <div class="col-sm-6"  id="card_div">
                                <div style="cursor:pointer;">
                                    <img src="FBimages/card.png" hidden="false"  id="card" draggable="true"  style="margin-left: auto; margin-right: auto;display: block;padding-top: 32px" >
                                </div>
                            </div>
                        </div><!-- ./ROW 2 -->
                    </div><!-- ./ITEMS -->
                </div>
                <!-- ./ITEMS COLUM -->
                
                <div class="col-sm-6 col-sm-pull-6" hidden="true" id="successD" >
                    <div class="items margin-bottom">
                        <img src="FBimages/deviceBig.png" id="device" draggable="false" ondrop="drop(event)" ondragover="allowDrop(event)" style="margin-left: auto; margin-right: auto;display: block;padding-top: 32px;padding-bottom: 18px;" >
                    </div>    
                </div>
            </div>
            
            <div class="row bottom">
                <div hidden="true" id="successP">
                    <div class="col-sm-5 col-sm-push-7">
                        <div class="more-info">
                            <p>شكراً لمشاركتك!
                            <br>ولمزيد من المعلومات برجاء الاتصال بخدمة عملاء أكيوتشيك
                            <br>يوميا من 9 صباحا و حتى 5 مساء على الأرقام التالية:
                            <br>من الخط الأرضي: <span dir="ltr">08000444444</span> (مجانا)
                            <br>أو <span dir="ltr">٢٢٩٢٦١٢٥</span> بسعر المكالمة العادية
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-7 col-sm-pull-5">
                        <div>
                            <h3 class="text-center">
                                تهانينا، تم إضافة اسمك إلى قائمة الفائزين المحتملين في مجموعة أكيو-تشيك بيرفورما وسيتم إعلامك إذا تم اختيارك.<br>
                                <span>سيتم سحب عشوائي في 14 نوفمبر2017.</span>
                            </h3>
                            <p>مجموعة أدوات أكيو-تشيك بيرفورما غير قابلة للتبديل ولا يمكن استردادها نقداً أو استبدالها بغيرها من الجوائز.</p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
        
        <script>
      function findTopLeft(element) {
          // console.log(element);
      var rec = element.getBoundingClientRect();
      return {top: rec.top + window.scrollY, left: rec.left + window.scrollX};
      } //call it like findTopLeft('#header');



      var step1 = false ,step2= false,step3= false;
      var cardDropped = false,pinDropped=false,handDropped=false;
      var item_coordinates = findTopLeft(document.getElementById('card'));
      // console.log(item_coordinates);
      var card_drag_status = false;
      var pin_drag_status = false;
      // console.log('===================================');
        $(function() {
          $( "#card" ).draggable();
          $( "#device_before" ).droppable({
            accept: "#card,#hand",
            tolerance: "touch",
            drop: function( event, ui ) {
             var draggable_item = ui.draggable[0].getAttribute('id');
             if(draggable_item=='card'){
                 $('#card').hide();
                 $("#device_before").fadeOut();
                        $('#device_before').fadeOut(100, function() {
                            var device = document.getElementById("dev_image");
                            device.src = "FBimages/deviceWithStripp.png";
                            device.style.padding = "10px 0px 0px 0px";
                            $('#device_before').fadeIn(500);
                        });
                  card_drag_status = true;
                  step1 = true;
             }
             else{
                 $('#hand_div').hide();
                 $("#device_before").fadeOut();
                        $('#device_before').fadeOut(100, function() {
                            var device = document.getElementById("dev_image");
                            device.src = "FBimages/deviceWithCardWithBlod.png";
                            device.style.padding = "10px 0px 0px 0px";
                            $('#device_before').fadeIn(500);
                        });
                         setTimeout(function(){
                            var paragraph = document.getElementById("successP");
                            paragraph.hidden = false;
                            
                            var successD = document.getElementById("successD");
                            successD.hidden = false;
                            
                            var items = document.getElementById("items");
                            items.hidden = true;
                            // alert("congrat !!!");
                            $.ajax({url: "/submit_win",
                            success: function(result){
                              window.setTimeout(function () {
                                    location.href = "/steps";
                                }, 5000);
                            }});
                        },1500);
             }
            },
            deactivate: function(event, ui){  
                  var draggable_item = ui.draggable[0].getAttribute('id');
                  if(draggable_item=='card'){

                      if(!card_drag_status) {
                        document.getElementById('card_div').innerHTML = '<div style="cursor:pointer;"><img src="FBimages/card.png" hidden="false" id="card" draggable="true" style="margin-left: auto; margin-right: auto; display: block; padding-top: 32px; position: relative;" class="ui-draggable"></div>';
                        $( "#card" ).draggable();

                      }
                  }
                  else{
                    document.getElementById('hand_div').innerHTML = '<img src="FBimages/handWithBlod.png" id="hand" draggable="true" style="margin-top: 32px;" class="ui-draggable" style="position: relative; padding: 10px 0px 0px;">';
                        $( "#hand" ).draggable();
                  }
            }
          });
        });


      $(function() {
          $( "#pin" ).draggable();
          $( "#hand_div" ).droppable({
            accept: "#pin",
            tolerance: "touch",
            drop: function( event, ui ) {
                 if(step1){
                     $('#pin').hide();
                     $("#hand_div").fadeOut();
                            $('#hand_div').fadeOut(100, function() {
                                var hand = document.getElementById("hand");
                                hand.src = "FBimages/handWithBlod.png";
                                hand.style.padding = "10px 0px 0px 0px";
                                $('#hand_div').fadeIn(500);
                            });
                      pin_drag_status = true;
                      step2 = true;
                      $( "#hand" ).draggable();
                }
            },
            deactivate: function(event, ui){  
              if(!pin_drag_status){
                document.getElementById('pin_div').innerHTML = '<img src="FBimages/pin.png" id="pin" draggable="true" style="margin-left: auto; margin-right: auto; display: block; padding-top: 25px;z-index: 9999999; position: relative;" class="ui-draggable">';
                $( "#pin" ).draggable();
            }
          }
          });
        });

         </script>

         <script type="text/javascript">
           if( !inIframe() && !is_mobile()){
            window.location.href = "https://www.facebook.com/TestPageBeeInter/app/2077796252440652/";
           }

           function inIframe () {
                    try {
                        return window.self !== window.top;
                    } catch (e) {
                        return true;
                    }
                }
                function is_mobile(){
                  var isMobile = false; 
                  if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
                      || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
                    return isMobile;

                }
         </script>
        </div>
    </body>
</html>