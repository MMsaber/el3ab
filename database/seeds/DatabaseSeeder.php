<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
         \App\User::create([
        'name' => 'admin',
        'email' => 'admin@beeieg.com',
        'fb_id' => '',
        'mobile' => '',
        'sugar_patient' => '',
        'user_medication' => '',
        'password' => Hash::make("G7FScXffs"),
      ]);

    }
}
