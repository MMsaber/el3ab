<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Cookie;
use App\User;
use Auth;


class WinController extends Controller
{
    public function index()
    {
    	$user = Auth::user();
    	if($user->winner=="Yes"){
    		return view("steps");
    	}
    	return view("win");
    }
    public function submit_win()
    {
    	$user = Auth::user();
    	$user->winner = "Yes";
    	$user->save();
    }
    public function steps()
    {
    	return view("steps");
    }				
}
