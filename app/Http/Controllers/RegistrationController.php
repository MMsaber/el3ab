<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Cookie;
use App\User;
use Auth;


class RegistrationController extends Controller
{
    public function save_user(Request $request)
    {
    	$fb_id = $request->cookie('fb_id');
    	$user_name = $request->input("user_name");
    	$user_mobile = $request->input("user_mobile");
    	$user_email = $request->input("user_email");
    	$sugar_patient = $request->input("sugar_patient");
    	$user_medication = $request->input("user_medication");
    	$check_user = User::where("fb_id" , $fb_id)->get()->first();
    	if($check_user){
    		Auth::login($check_user , true);
            return redirect("win");
    	}
    	else{
    	$user = new User;
    	$user->name = $user_name;
    	$user->mobile = $user_mobile;
    	$user->fb_id = $fb_id;
    	$user->email = $user_email;
    	$user->sugar_patient = $sugar_patient;
    	$user->user_medication = $user_medication;
    	$user->save();
    	Auth::login($user , true);
        return redirect("win");
    }
    }
     public function index(){
        if(Auth::check()){
           return redirect ("win"); 
        }
        else{
            return view ("registration");
        }
     }
}
