<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Http\Controllers\Controller;
use Socialite;
use Cookie;

class SocialAuthController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();   
    }
        public function redirect_to()
    {
        if(Cookie::get("fb_id")){
        return redirect("/registration");
        }
        else{
            return view("home");
        }   
    }   

    public function callback()
    {
        $providerUser = \Socialite::driver('facebook')->user();
        Cookie::queue("fb_id", $providerUser->getId() , 60 * 24 * 30);
        Cookie::queue("user_email", $providerUser->getEmail() , 60 * 24 * 30);
        Cookie::queue("user_name", $providerUser->getName() , 60 * 24 * 30);
        return redirect("/registration");

    }
}