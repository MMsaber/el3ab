<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
class ReportController extends Controller
{
    public function show(){
    	if(!Auth::check()){
    		return redirect("login");
    	}
    	$user = Auth::user();
    	if($user->id == 1){
    		$users = User::where('id', '!=', Auth::id())->get();
    		return view("report")->with("users" , $users);
    	}
    	else{
    		return redirect("login");
    	}
    }
}
